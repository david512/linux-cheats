#!/bin/sh

GRUB_ROOT_DIR="/mnt/grub"
EFI_DIR="/mnt/efi"

if [ ! -d $GRUB_ROOT_DIR ]; then
  sudo mkdir $GRUB_ROOT_DIR
fi

sudo mount /dev/disk/by-partlabel/xps_grub $GRUB_ROOT_DIR


if [ ! -d $EFI_DIR ]; then
  sudo mkdir $EFI_DIR
fi

sudo mount /dev/nvme0n1p2 $EFI_DIR