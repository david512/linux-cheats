#!/bin/sh

MNT_DIR="/mnt/xps_system"

if [ ! -d $MNT_DIR ]; then
  sudo mkdir $MNT_DIR
fi

sudo mount /dev/disk/by-partlabel/xps_system $MNT_DIR
