#!/bin/sh

echo "Type system name:"
read SYS_NAME

MNT_DIR='/mnt/'$SYS_NAME

if [ ! -d $MNT_DIR ]; then
  sudo mkdir $MNT_DIR
fi

sudo mount /dev/disk/by-partlabel/xps_system -o subvol='@'$SYS_NAME $MNT_DIR
#cd $MNT_DIR
#sudo mount /dev/disk/by-partlabel/xps_system -o subvol='@'$SYS_NAME'_home' home

#BOOT_PART='/dev/disk/by-partlabel/xps_boot_'$SYS_NAME

#if [ -L $BOOT_PART ]; then
#  sudo mount $BOOT_PART boot
#else
#  echo "Type boot partition name:"
#  read BOOT_PART_NAME
#  sudo mount /dev/disk/by-partlabel/$BOOT_PART_NAME boot
#fi

#cd boot
#sudo mount '/dev/disk/by-partlabel/EFI\x20system\x20partition' efi