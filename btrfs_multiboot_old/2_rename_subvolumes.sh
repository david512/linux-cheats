#!/bin/sh

MNT_DIR="/mnt/xps_system"

cd $MNT_DIR

echo "Type new system name:"
read NEW_NAME

if [ -d @ ]; then
  sudo mv @ '@'$NEW_NAME
fi

if [ -d @home ]; then
  sudo mv @home '@'$NEW_NAME'-home'
fi

if [ -d @cache ]; then
  sudo mv @cache '@'$NEW_NAME'-cache'
fi

if [ -d @log ]; then
  sudo mv @log '@'$NEW_NAME'-log'
fi
