Komendy vima

#otwórz plik
:e nazwa_pliku

#otwarcie pliku w nowej karcie
:tabe nazwa_pliku

#otwarcie pliku na podzielonym ekranie (split screen)
:split <nazwa_pliku>
:vsplit <nazwa_pliku>

#przełaczanie pomiędzy panelami
ctrl + w + [h,j,k,l]

#przełączanie zakładek
g + t, shift + g + t

#włączenie obsługi myszy
:set mouse=a

#kolorowanie składni
:syntax on

#typ pliku dla kolorowania składni
:set filetype=<typ_pliku>

#zestaw kolorów dla ciemnego tła
:set background=dark

#automatyczne wcięcia
:set autoindent

#uzupełnianie składni
ctrl + n

#kopiuj linię (bierzącą)
yy

#wklej linię (poniżej bierzącej)
p

#wklej 100 razy
100p

#usuń linię
dd

#usuń dwie linie (od bierzącej w dół)
d2d

#idź na początek pliku
gg

#idź na koniec pliku
G

#skocz do drugiej klamry, nawiasu
%

#cofnij (undo)
u

#powtórz (redo)
ctrl + r

#usuń wszystkie znaki pomiędzy podanymi wybranymi znakami
#i wejdź w tryb edycji (np. ci' (pomiędzy apostrofami))
ci<znak>

#usuń pomiędzy tagami (np. html)
cit

#zaznaczanie linii
shift + v

#sortowanie zaznaczonych linii
shift + :
:'<,'>sort

#ustawienie markera (np. ma)
m<litera>

#idź do markera (np. `a)
`<litera>

#zwiększ wcięcie zaznaczenia (>)
shift + .

#zmniejsz wcięcie zaznaczenia (<)
shift + ,

#zamień bierzący znak na
r<znak>

#zamień kilka znaków na (np. 4rx)
<liczba>r<znak>

#numerowanie linii
:set number
:set nonumber

#wyszukiwanie
/szukana_fraza

#znajdź nastęny
n

#podświetlanie znalezionych
:set hlsearch
:nohl

#incrementalne wyszukiwanie
:set incsearch

#uruchamianie poleceń z poziomu vima
#(% dla bierzącego pliku tekstowego) np :!python %
:!<nazwa_programu> %

#mapowanie klawiszy (np. :map <F5> :!python %)
:map <klawisz> :<polecenie>

#mapowanie dla trybu wprowadzania (np. :imap =amk ala ma kota)
:imap <fraza1> <fraza2>

#zapisz
:w

#zakończ
:q

#wyjdź i zapisz wszystkie otwarte pliki
:wqa