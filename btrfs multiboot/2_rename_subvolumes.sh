#!/bin/sh

MNT_DIR="/mnt/btrfs"
cd $MNT_DIR

if [ ! -d @ ]; then
  echo 'No new subvolumes'
  exit 1
fi

#echo "Type new system name:"
NEW_NAME=$1

if [ -d @ ]; then
  sudo mv @ '@'$NEW_NAME
fi

if [ -d @home ]; then
  sudo mv @home '@'$NEW_NAME'-home'
fi

if [ -d @cache ]; then
  sudo mv @cache '@'$NEW_NAME'-cache'
fi

if [ -d @log ]; then
  sudo mv @log '@'$NEW_NAME'-log'
fi
