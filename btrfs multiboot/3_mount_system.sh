#!/bin/sh

#if [ ! -d $1 ]; then
#	echo 'No device'
#	exit 1
#fi

DEVICE=$1
SYS_NAME=$2

if [ ! -d '/mnt/btrfs/@'$SYS_NAME ]; then
	echo 'No subvolume. Exiting'
	exit 1
fi

MNT_DIR='/mnt/subvol'

if [ ! -d $MNT_DIR ]; then
  sudo mkdir $MNT_DIR
fi

sudo mount $DEVICE -o subvol='@'$SYS_NAME $MNT_DIR

