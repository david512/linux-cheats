#!/bin/sh

cd /mnt/btrfs
DIR=$1

if [ ! -d $DIR ]; then
	echo 'Brak katalogu'
	exit 1
fi

if [ ! `stat --format=%i $DIR` -eq 256 ]; then
	echo 'Converting '$DIR
	echo 'Renaming to '$DIR'_original'
	sudo mv $DIR $DIR'_original'
	sudo btrfs subvolume create $DIR
	echo 'Copying data...'
	sudo cp --archive --one-file-system --reflink=always $DIR'_original/.' $DIR
	echo 'Removing original...'
	sudo rm -rf --one-file-system $DIR'_original'
	echo 'Done! :)'
else
	echo $1' is subvolume'
	exit 1
fi
