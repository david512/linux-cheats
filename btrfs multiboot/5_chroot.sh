#!/bin/sh

MNT_DIR='/mnt/subvol'

if [ -d $MNT_DIR ]; then
  sudo mount --rbind /dev $MNT_DIR'/dev'
  sudo mount --rbind /proc $MNT_DIR'/proc'
  sudo mount --rbind /sys $MNT_DIR'/sys'
  sudo chroot $MNT_DIR
fi
