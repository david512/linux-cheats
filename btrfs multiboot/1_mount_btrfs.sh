#!/bin/sh

#if [ ! -d $1 ]; then
#	echo 'No device'
#	exit 1
#fi

MNT_DIR="/mnt/btrfs"

if [ ! -d $MNT_DIR ]; then
  sudo mkdir $MNT_DIR
fi

sudo mount $1 $MNT_DIR
