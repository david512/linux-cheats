#!/bin/sh

if [ ! -d $1 ]; then
	echo 'No device'
	exit 1
fi

DEVICE=$1
MNT_DIR="/mnt/btrfs"

if [ ! -d $MNT_DIR ]; then
  sudo mkdir $MNT_DIR
fi

sudo mount $DEVICE $MNT_DIR

#--------------------------

cd $MNT_DIR

if [ ! -d @ ]; then
  echo 'No new subvolumes'
  exit 1
fi

echo "Type new system name:"
read NEW_NAME

if [ -d @ ]; then
  sudo mv @ '@'$NEW_NAME
fi

if [ -d @home ]; then
  sudo mv @home '@'$NEW_NAME'-home'
fi

if [ -d @cache ]; then
  sudo mv @cache '@'$NEW_NAME'-cache'
fi

if [ -d @log ]; then
  sudo mv @log '@'$NEW_NAME'-log'
fi

#-------------------------------------------

echo "Type system name:"
read SYS_NAME

SYS_MNT_DIR='/mnt/'$SYS_NAME

if [ ! -d $SYS_MNT_DIR ]; then
  sudo mkdir $SYS_MNT_DIR
fi

sudo mount $DEVICE -o subvol='@'$SYS_NAME $SYS_MNT_DIR

