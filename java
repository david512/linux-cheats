
OpenSource

	sudo apt install default-jre
or
	sudo apt install openjdk-8-jre


Oracle

	sudo apt-add-repository ppa:webupd8team/java
	sudo apt update
	sudo apt install oracle-java8-installer

or

	sudo add-apt-repository ppa:linuxuprising/java
	sudo apt-get install oracle-java10-installer

Once installed, Oracle Java 10 should be automatically set as default. If not, run command:

	sudo apt-get install oracle-java10-set-default


Uninstall Oracle Java

	sudo apt-get remove oracle-java8-installer
	sudo apt-get purge oracle-java8-installer
	sudo apt-get autoremove
