#include <LiquidCrystal.h>

/* LCD settings */
//#define CONTRAST  23
//#define BACKLIGHT 28836

/* LCD pins */
#define PIN_CONTRAST  6
#define PIN_BACKLIGHT 9

#define PIN_RS 8
#define PIN_EN 9
#define PIN_D4 4
#define PIN_D5 5
#define PIN_D6 6
#define PIN_D7 7

/* Time until backlight is turned off if no update is received */
#define SCREEN_OFF_DELAY  10000

LiquidCrystal lcd(PIN_RS, PIN_EN, PIN_D4, PIN_D5, PIN_D6, PIN_D7);

String inData = "";            // String for buffering the message
//boolean stringComplete = false;     // Indicates if the string is complete
//int curFanChar = 0;                 // Current character of fan animation
//unsigned long previousUpdate = 0;   // Long to keep the time since last received message

byte celsius[] = {
  0b01000,
  0b10100,
  0b01000,
  0b00011,
  0b00100,
  0b00100,
  0b00011,
  0b00000
};

void setup() {
  // Setup LCD
  lcd.begin(16, 2);

  // Setup serial
  Serial.begin(9600);

  // Create the custom characters
  lcd.createChar(1, celsius);
//  lcd.clear();
//  lcd.setCursor(0,0);
//  lcd.write("xccc");
//  lcd.write(1);
//  lcd.cursor();
//  lcd.blink(); 
}

void loop() {
  while (Serial.available() > 0) {
    char received = Serial.read();
    inData += received;

    if (received == '|') {
      inData = inData.substring(0, inData.length()-1);
      while (inData.length() < 32) {
        inData += " ";
      }

      inData.replace("^", "\1");
      lcd.cursor();
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print(inData.substring(0, 16));
      lcd.setCursor(0,1);
      lcd.print(inData.substring(16, 32));

      inData = "";
    }

  }
}
