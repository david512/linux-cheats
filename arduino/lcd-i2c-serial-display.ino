#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,20,4);  // set the LCD address to 0x27 for a 16 chars and 2 line display

String inData = "";            // String for buffering the message
String st = "";

byte celsius[] = {
  0b01000,
  0b10100,
  0b01000,
  0b00011,
  0b00100,
  0b00100,
  0b00011,
  0b00000
};

void setup() {
  // Setup LCD
  lcd.init();
//  lcd.backlight();

  // Setup serial
  Serial.begin(9600);

  // Create the custom characters
  lcd.createChar(1, celsius);
//  lcd.clear();
//  lcd.setCursor(0,0);
//  lcd.write("xccc");
//  lcd.write(1);
//  lcd.cursor();
//  lcd.blink(); 
}

void disp(String str) {
  while (str.length() < 80) {
    str += " ";
  }
  str.replace("^", "\1");
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print(str.substring(0, 20));
  lcd.setCursor(0,1);
  lcd.print(str.substring(20, 40));
  lcd.setCursor(0,2);
  lcd.print(str.substring(40, 60));
  lcd.setCursor(0,3);
  lcd.print(str.substring(60, 80));
}

void settings(String str) {
  if (str == "ON") {
    lcd.backlight();
  } else if (str == "OFF") {
    lcd.noBacklight();
  } else if (str == "CLR") {
    lcd.clear();
  }
}

void loop() {
  while (Serial.available() > 0) {
    char received = Serial.read();
    inData += received;

    if (received == '|') {
//      inData = inData.substring(0, inData.length()-1);
      String instr = inData.substring(0, 3);
//      Serial.print(instr); 
      inData = inData.substring(3, inData.length()-1);

      if (instr == "DSP") {
        disp(inData);
      } else if (instr == "SET") {
        settings(inData);
      }
      
      inData = "";
    }

  }
}
