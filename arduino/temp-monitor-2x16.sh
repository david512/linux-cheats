#!/bin/sh

stty -F /dev/ttyACM0 cs8 9600 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts -hupcl

echo "Arduino Temperature Monitor.\nPress CRTL+C for Exit."

while [ true ]; do

SENS=$(sensors)
CPU=$(echo $SENS | grep -oP 'Tdie.*?\+\K[0-9.]+')
GPU=$(echo $SENS | grep -oP 'junction.*?\+\K[0-9.]+')
MEM=$(echo $SENS | grep -oP 'mem.*?\+\K[0-9.]+')
NVME=$(sudo nvme smart-log /dev/nvme0n1 | grep -oP 'temperature.*?\: \K[0-9.]+')

#echo "GPU: $GPU°C, MEM: $MEM°C, CPU: $CPU°C, NVME: $NVME°C"

#   1234567890123456
L1="GPU ${GPU%.*}^ MEM ${MEM%.*}^"
L2="CPU ${CPU%.*}^ NVME $NVME^"

if [ ${#L1} -lt 16 ]; then
	while [ ${#L1} -lt 16 ]; do
		L1="$L1 "
	done
elif [ ${#L1} -gt 16 ]; then
	L1=${L1:0:16}
fi

BUF="$L1$L2"

#echo $BUF

echo -n "$BUF|" > /dev/ttyACM0

sleep 1

done
