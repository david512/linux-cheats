#!/bin/bash

stty -F /dev/ttyACM0 cs8 9600 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts -hupcl

echo "Arduino Temperature Monitor."
echo "Press CRTL+C for Exit."

echo -n "SETON|" > /dev/ttyACM0

while [ true ]; do

SENS=$(sensors)
CPU=$(echo $SENS | grep -oP 'Tdie.*?\+\K[0-9.]+')
GPU=$(echo $SENS | grep -oP 'junction.*?\+\K[0-9.]+')
MEM=$(echo $SENS | grep -oP 'mem.*?\+\K[0-9.]+')
NVME=$(sudo nvme smart-log /dev/nvme0n1 | grep -oP 'temperature.*?\: \K[0-9.]+')

#echo "GPU: $GPU°C, MEM: $MEM°C, CPU: $CPU°C, NVME: $NVME°C"

L1="Grafika: ${GPU%.*}^"
L2="Pamiec graf.: ${MEM%.*}^"
L3="Procesor: ${CPU%.*}^"
L4="Dysk: $NVME^"

#   12345678901234567890123456789012345678901234567890123456789012345678901234567890
#	Grafika: 52^        Pamiec graf.: 70^   Procesor: 33^       Dysk: 41^           


if [ ${#L1} -lt 20 ]; then
	while [ ${#L1} -lt 20 ]; do
		L1="$L1 "
	done
elif [ ${#L1} -gt 20 ]; then
	L1=${L1:0:20}
fi

if [ ${#L2} -lt 20 ]; then
	while [ ${#L2} -lt 20 ]; do
		L2="$L2 "
	done
elif [ ${#L2} -gt 20 ]; then
	L2=${L2:0:20}
fi

if [ ${#L3} -lt 20 ]; then
	while [ ${#L3} -lt 20 ]; do
		L3="$L3 "
	done
elif [ ${#L3} -gt 20 ]; then
	L3=${L3:0:20}
fi

if [ ${#L4} -lt 20 ]; then
	while [ ${#L4} -lt 20 ]; do
		L4="$L4 "
	done
elif [ ${#L4} -gt 20 ]; then
	L4=${L4:0:20}
fi

BUF="DSP$L1$L2$L3$L4"

#echo $BUF

echo -n "$BUF|" > /dev/ttyACM0

sleep 1

done
